#ifndef STRUKTURA_H_INCLUDED
#define STRUKTURA_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct lista{
      int id; //!< element struktury przechowojacy kolejne numer id
      char imie[20];//!<element struktury przechowujacy kolejne imiona
      char nazwisko[20];//!<element struktury przechowujacy kolejne nazwiska
      char telefon[14];//!<element struktury przechowujacy kolejne numer telefonu
      char miasto[40];//!<element struktury przechowujacy kolejne miasta
      char ulica[60];//!<element struktury przechowujacy kolejne ulice
      char poczta[7];//!<element struktury przechowujacy kolejne poczta
      struct lista* next;//!<wskaznik na strukture kt�ry wskazuje na nastepny element listy
      struct lista* prev;//!<wskaznik na strukture kt�ry wskazuje poprzedni element listy
      struct lista* pierwszy; //!<wskaznik na strukture ktory wskazuje na pierwszy element listy
  };
#endif // STRUKTURA_H_INCLUDED
