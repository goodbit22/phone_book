#ifndef DODAWANIE_PLIK_H_INCLUDED
#define DODAWANIE_PLIK_H_INCLUDED

#include "../struktura.h"


void dodaj_z_pliku(struct lista** kontakt);
void dodaj_do_pliku(struct lista* kontakt);

#endif // DODAWANIE_PLIK_H_INCLUDED
