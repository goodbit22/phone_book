#include "dodawanie_standardowe.h"
#include "dodawanie_plik.h"



/*!
 *\brief Funkcja dodaje kontakty do listy
 *\param struct list** kontakt
 *\details wskaznik podwojny na strukture list umozliwia nam to operacje na oryginale

 */
 void dodaj(struct lista** kontakt){
    struct lista* nowy,*wsk;
    nowy=(struct lista*)malloc(sizeof(struct lista));
    wsk=(*kontakt);
    if(wsk->next==NULL){
        nowy->id=0;
        }
    else{
        nowy->id=(wsk->id)+1;
        };

    nowy->prev=NULL;
    printf("Imie\n");
    scanf("%s",nowy->imie);
    printf("Nazwisko\n");
    scanf("%s",nowy->nazwisko);
    printf("Numer telefonu\n");
    scanf("%s",nowy->telefon);
    printf("Miasto\n");
    scanf("%s",nowy->miasto);
    printf("Ulica\n");
    scanf("%s",nowy->ulica);
    printf("Numer pocztowy\n");
    scanf("%s",nowy->poczta);
    nowy->next=wsk;
    wsk->prev=nowy;
    (*kontakt)=nowy;
    wsk=NULL;
    free(wsk);
}
/*!
*\brief  funckja sprawdza czy uzytkownik chce dodac osobe do ksiazki  i wysyla do funckji dodaj zmienna indentyfikator i wartosc wskaznika tel
*\param struct lista** tel
*\details podowj wskaznik na struktura umozliwia prace na oryginale
*/
void menu_dodawanie_standardowe(struct lista** tel){


    char  wybor[3];
	do{
    	printf("Czy chcesz dodać osobe ");
	scanf("%s",wybor);
	if(!strcmp(wybor,"tak")){
		dodaj(tel);
		}
	else if(!strcmp(wybor,"nie")){
		break;
	}
	else{
		puts("nie wiadomo co robic");
		}
  } while(strcmp(wybor,"nie"));
  dodaj_do_pliku(*tel);
}
