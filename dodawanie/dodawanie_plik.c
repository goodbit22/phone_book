/*!
* \authors Hagaven goodbit22
* \date 2019-04-02
* \section intro_sec Opis
*dodawanie  osób z pliku i recznie
* \mainpage Ogólne informacje
*/

#include "dodawanie_plik.h"


/*!
*\brief funkcja odczytuje plik test.txt i przypisuje  jego zawartość  do struktury lista_dodawanie_plik
\param struct lista_dodawanie_plik** kontakt
\details podwojny wskaznik na strukture lista_dodawanie_plik o nazwie kontakt umozliwia nam operacje na oryginale


*/
void dodaj_z_pliku(struct lista** kontakt){
      struct lista* nowy,*wsk;
      int c;

      FILE* plik=fopen("ksiazka.txt","r");
      while(1){
            wsk=(*kontakt);
            if(wsk->next==NULL){
                wsk->id=0;
            }
            else{
                wsk->id=(wsk->next->id)+1;
            }
            if ((c=fgetc(plik)) != EOF){
                    ungetc(c,plik);
                if(wsk==NULL){
     fscanf(plik,"%s %s %s %s %s %s\n",(*kontakt)->imie,(*kontakt)->nazwisko,(*kontakt)->telefon,(*kontakt)->miasto,(*kontakt)->ulica,(*kontakt)->poczta);
                }
                else{
                    nowy=(struct lista*)malloc(sizeof(struct lista));
      nowy->prev=NULL;
     nowy->next=wsk;
     nowy->pierwszy=wsk->pierwszy;
     fscanf(plik,"%s %s %s %s %s %s\n",nowy->imie,nowy->nazwisko,nowy->telefon,nowy->miasto,nowy->ulica,nowy->poczta);
      wsk->prev=nowy;
      (*kontakt)=nowy;
                }
}
else
    break;
      }
wsk=NULL;
      free(wsk);
      fclose(plik);
  }

/*!
*\brief funkcja zapisuje zawartość  struktury lista_dodawanie_plik  do pliku ksiazka.txt
\param struct lista_dodawanie_plik kontakt
\details wskaznik na sturkture lista_dodawanie_plik o nazwie kontakt
*/
void dodaj_do_pliku(struct lista* kontakt){
    struct lista* wsk=kontakt;
    FILE* plik=fopen("ksiazka.txt","w");
    while(wsk!=NULL){
        fprintf(plik," %s %s %s %s %s %s\n",wsk->imie,wsk->nazwisko,wsk->telefon,wsk->miasto,wsk->ulica,wsk->poczta);
	wsk=wsk->next;
    };
    wsk=NULL;
    free(wsk);
    fclose(plik);
}


