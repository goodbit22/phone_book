$CC=gcc

all: output

dodawanie_standardowe.o:  dodawanie/dodawanie_standardowe.c dodawanie/dodawanie_standardowe.h struktura.h 
	$(CC) -c dodawanie/dodawanie_standardowe.c 

dodawanie_plik.o: dodawanie/dodawanie_plik.c dodawanie/dodawanie_plik.h
	$(CC) -c dodawanie/dodawanie_plik.c

wyswietl.o: wyswietlanie/wyswietl.c wyswietlanie/wyswietl.h wyswietlanie/wyswietlanie_standardowe.h
	$(CC) -c wyswietlanie/wyswietl.c 

wyswietlanie_standardowe.o: wyswietlanie/wyswietlanie_standardowe.c wyswietlanie/wyswietlanie_standardowe.h
	$(CC) -c wyswietlanie/wyswietlanie_standardowe.c

edytowanie.o: edytowanie/edytowanie.c edytowanie/edytowanie.h struktura.h wyswietlanie/wyswietlanie_standardowe.h
	$(CC) -c edytowanie/edytowanie.c

usuwanie.o: usuwanie/usuwanie.c usuwanie/usuwanie.h
	$(CC) -c usuwanie/usuwanie.c

wyszukiwanie.o: wyszukiwanie/wyszukiwanie.c wyszukiwanie/wyszukiwanie.h
	$(CC) -c wyszukiwanie/wyszukiwanie.c 

main.o: main.c struktura.h 
	$(CC) -c main.c 
	
output: dodawanie_standardowe.o dodawanie_plik.o edytowanie.o usuwanie.o wyswietl.o wyswietlanie_standardowe.o  wyszukiwanie.o main.o
	$(CC) main.o dodawanie_standardowe.o dodawanie_plik.o edytowanie.o usuwanie.o wyswietl.o wyswietlanie_standardowe.o  wyszukiwanie.o -o main

clean:
	rm *.o 
	rm -f main
