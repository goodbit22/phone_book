
#include "../struktura.h"
/*!
* \authors Hagaven goodbit22
* \date 2019-04-02
* \section intro_sec Opis
*wyszukiwanie kontaktow
* \mainpage Ogólne informacje
*/


/*!
*\brief funkcja umozliwia zapis do struktury z pliku  kontaktu
*\param struct lista** kontakt
*\details wskaznika podwojny do struktury umozliwia besposrednie operowanie na  oryginalach pol struktury
*/


void wczytaj_plik(struct lista **kontakt){
 struct lista* nowy,*wsk;
      wsk=(*kontakt);
      FILE* plik=fopen("test.txt","r");

//      int id;
      char imie[20];
      char nazwisko[20];
      char telefon[15];
      char miasto[40];
      char ulica[60];
      char poczta[11];

      fscanf(plik," %s  %s %s  %s %s %s ",imie,nazwisko,telefon,miasto,ulica,poczta);
      nowy=malloc(sizeof(struct lista));
      nowy->prev=NULL;

      strncpy(nowy->imie,imie,20);
      strncpy(nowy->nazwisko,nazwisko,20);
      strncpy(nowy->telefon,telefon,11);
      strncpy(nowy->miasto,miasto,40);
      strncpy(nowy->ulica,ulica,60);
      strncpy(nowy->poczta,poczta,7);


     nowy->next=wsk;
      wsk->prev=nowy;
      (*kontakt)=nowy;
      wsk=NULL;
      free(wsk);
      fclose(plik);
}


/*!
*\brief funkcja umozliwia wyswietlanie kontaktow
*\param struct lista* kontakt
*\details wskaznika  do struktury
*/

void odczyt(struct lista* kontakt){
    struct lista* wsk=kontakt;
    while(wsk->prev!=NULL){
	wsk=wsk->prev;
    };
    while(wsk->next!=NULL){
        printf("%s %s %s %s %s %s  \n",wsk->imie,wsk->nazwisko,wsk->telefon,wsk->ulica,wsk->poczta,wsk->miasto);
        wsk=wsk->next;
    };
}
/*!
*\brief funkcja umozliwia wyswietlanie kontaktow
*\param struct lista* kontakt
*\details wskaznika do  struktury
*/
void wyszukaj(struct lista * kontakt){

    char imie[20];
    char nazwisko[20];
    int znaleziono=0;

    printf("Imie\n");
    scanf("%s",imie);
    printf("Nazwisko\n");
    scanf("%s",nazwisko);


struct lista* wsk=kontakt;

      while(wsk->prev!=NULL){
          wsk=wsk->prev;
      };
      while(wsk->next!=NULL){
          if(!strcmp(imie,wsk->imie) && !strcmp(nazwisko,wsk->nazwisko)){
	  	puts("Znaleziono uzytkowanika\n");
		printf("%s %s %s %s %s %s  \n",wsk->imie,wsk->nazwisko,wsk->telefon,wsk->ulica,wsk->poczta,wsk->miasto);
		znaleziono=1;

	  }
	  wsk=wsk->next;

      };
      if(znaleziono==0){
        printf("Nie znaleziono uzytkownika\n");
      }
}
/*!
*\brief funkcja umozliwia wysyla wskaznik do funckcji odczyt i wyszukaj
*\param struct lista** kontakt
*\details wskaznika podwojny do struktury umozliwia besposrednie operowanie na  oryginalach pol struktury
*/
void menu_wyszukiwanie(struct lista **kontakt){

char  wybor[3];
	do{
	//identyfikator = identyfikator + 1;
    	printf("Czy chcesz wyszukac  osobe ");
	scanf("%s",wybor);
	if(!strcmp(wybor,"tak")){
		wyszukaj(*kontakt);
		}
	else if(!strcmp(wybor,"nie")){
		break;
	}
	else{
		puts("nie wiadomo co robic");
		}
  } while(strcmp(wybor,"nie"));

}


