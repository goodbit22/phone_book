#ifndef WYSZUKIWANIE_H_INCLUDED
#define WYSZUKIWANIE_H_INCLUDED
#include "../struktura.h"
void wczytaj_plik(struct lista **kontakt);
void odczyt(struct lista* kontakt);
void wyszukaj(struct lista * kontakt);
void menu_wyszukiwanie(struct lista **kontakt);

#endif // WYSZUKIWANIE_H_INCLUDED
