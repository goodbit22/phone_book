#include "dodawanie/dodawanie_plik.h"
#include "dodawanie/dodawanie_standardowe.h"
#include "edytowanie/edytowanie.h"
#include "usuwanie/usuwanie.h"
#include "wyswietlanie/wyswietl.h"
#include "wyswietlanie/wyswietlanie_standardowe.h"
#include "wyszukiwanie/wyszukiwanie.h"
#include "struktura.h"
#include "uchar.h"
#include <locale.h>
#include <unistd.h>

/*!
* \authors Hagaven, goodbit22
* \date 2019-03-31
* \section intro_sec Opis
*glowny plik
* \mainpage Og�lne informacje
*\bug bledne dzialanie  pliku makefile
*/

//prototypy funckji
void menu(void);
/*!
*\brief funkcja umozliwia wybor opcji co chcesz zrobic w ksiazce telefonicznej   (nie dodano funkcji z katalogow )
*funkcja nie przyjmuje zadnych parametr�w


*/

void clearScreen(void){
  const char *CLEAR_SCREEN_ANSI = "\e[1;1H\e[2J";
  write(STDOUT_FILENO, CLEAR_SCREEN_ANSI, 12);
}

int main(void){
    struct lista* tel=(struct lista*)malloc(sizeof(struct lista));
    tel->pierwszy=tel;
    tel->next=NULL;
    tel->id=0;
    setlocale(LC_ALL,"Polish");
	int znak;
 	int znak_dod;
	int znak_wys;
	while(znak != 6){
		clearScreen();
		menu();
		scanf("%d",&znak);
		switch(znak){
			case 1:
				clearScreen();
				znak_dod=0;
				puts("Wybrano dodawanie");
				//zawartosc katalogu dodawanie
				while(znak_dod != 4){
					//	menu();
					clearScreen();
					printf("1.Dodawanie z pliku\n2.Dodawanie standardowe\n3.Dodawanie do pliku\n4.Powrot\n");
					printf("\n");
					scanf("%d",&znak_dod);
					switch(znak_dod){
						case 1:
							puts("Wybrano dodawanie z pliku");
							//dodawanie dodawanie_plik1.c
							dodaj_z_pliku(&tel);
							break;
						case 2:
							puts("Wybrano dodawanie standardowe");
							//dodaj dodawanie_standardowe.c
							menu_dodawanie_standardowe(&tel);
							break
							;
                        case 3:
                            dodaj_do_pliku(tel);
                            break;

						case 4:
							//exit(1);
							break
							;
						default:
							printf("nie ma takiej opcji w menu\n");
							break
							;

					}
				}
				break;


			case 2:
				clearScreen();
				puts("Wybrano usuwanie");
				//zawartosc katalogu usuwanie
				menu_usuwanie(&tel);
				break;

			case 3:
				clearScreen();
				puts("Wybrano edytowanie");
				menu_edytowanie(&tel);
				break;

			case 4:
						clearScreen();
						puts("Wybrano wyswietlanie");
						znak_wys=0;
                while(znak_wys!=3){
                    printf("1.Drukuj aktualna liste\n2.Wyswietl posortowana liste\n3.Powrot");
                    printf("\n");
                    scanf("%d",&znak_wys);
                    switch(znak_wys){
                case 1:
										clearScreen();
                    wydrukuj(tel);
                    break;
                case 2:
										clearScreen();
                    wydrukuj(posortowana_lista(&tel));
                    break;
                case 3:
										clearScreen();
                    break;
                default:
                    printf("Nieprawidlowe polecenie\n Sproboj ponownie");
                    break;
				}

                }
                znak_wys=0;
                break;


            case 5:
								clearScreen();
                menu_wyszukiwanie(&tel);
                break;
			case 6:
				clearScreen();
				break;
			default:
				clearScreen();
				printf("nie ma takiej opcji w menu\n");
				system("PAUSE");
				break;


		}
	}
	free(tel);
return 0;
}
/*!
*\brief funkcja wyswietla menu tekstowe
*funkcja nie przyjmuje zadnych parametr�w
*/
void menu(void){
	puts("Menu ksiazki telefonicznej");
	puts("1.Dodawanie kontaktow");
	puts("2.Usuwanie kontaktow");
	puts("3.Edytowanie  kontaktow");
	puts("4.Wyswietlanie kontaktow");
	puts("5.Wyszukiwanie kontaktow");
	puts("6.Wyjscie");

}
