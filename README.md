## PhoneBoook
A simple phone book in C 
![project](./img/graphic.png)

## Features
* adding contacts from a file 
* adding contacts in the standard way 
* deleting contacts
* displaying contacts
* editing contacts
* searching for contacts

## Table of Contents 
* [Features](#fea)
* [Running and installation](#run)
* [Technologies](#tech)
* [Authors](#authors)

## Running and installation

### Compilation
To compile the project you just need to type command in the terminal
```
	make
```

### Running
```
	./main
```
## Technologies
Project is created with:
* C
* Doxygen

## Authors
*****
__goodbit22__ --> https://gitlab.com/users/goodbit22
*****
__hagaven__   --> https://gitlab.com/hagaven
*****
