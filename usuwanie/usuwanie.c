
#include "usuwanie.h"
/*!
   *\authors Hagaven goodbit22
   *\date 2019-04-02
   *\section intro_sec Opis
   *usuwanie osob z ksiazki
   *\mainpage Ogólne informacje
 */

/*!
*\brief funkcja usuwa kontakt
\param struct list** kontakt
\details  podwoj wskaznik na  strukture umozliwia operowanie na oryginalych polach
*/
void usun(struct lista** kontakt){
    struct lista* wsk=(*kontakt);
    wsk->prev->next=wsk->next;
    //wsk->next=wsk->prev;
    free(wsk);
    wsk=NULL;
}
/*!
*\brief wyswietla komunikt czy usunac kontakt  czy nie a jesli tak to pyta nasz jaki kontakt usunac  i wywoluje funckje usun z odpowiednim parametrem
\param struct list** kontakt
\details  podwoj wskaznik na  strukture umozliwia operowanie na oryginalych polach
*/
void menu_usuwanie(struct lista** kontakt){
char  wybor[3];
	do{
    	printf("Czy chcesz usunac osobe ");
	scanf("%s",wybor);
	if(!strcmp(wybor,"tak")){

        printf("który kontakt chcesz usunać:");
        int kolejny_wybor;
        scanf("%d",&kolejny_wybor);

        int i=0;
        struct lista*wsk=(*kontakt);
        for(i=0;i<kolejny_wybor; i++){
            wsk = wsk->next;
        }
                usun(&wsk);
                wsk=NULL;
                free(wsk);
		     //usun(kontakt);
		}
	else if(!strcmp(wybor,"nie")){
		break;
	}
	else{
		puts("nie wiadomo co robic");
		}
  } while(strcmp(wybor,"nie"));
}
