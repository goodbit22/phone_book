/*!
* \authors Hagaven goodbit22
* \date 2019-04-02
* \section intro_sec Opis
*wyswietlnaie  osób standardowo rosnaco i malejaco
* \mainpage Ogólne informacje
*/

#include "wyswietlanie_standardowe.h"


/*!
*\brief funkcja wsywietla na ekran kontakty zawarte w strukturze list
\param struct list** kontakt
\details wskaznik  na strukture umozliwia operacje na kopi

*/

void wydrukuj(struct lista* kontakt){
    struct lista* wsk=kontakt;
    int i=0;
    if(wsk!=NULL){
        if(wsk->next!=NULL)
        while(wsk->next!=NULL){

            printf("%d.%s %s %s %s %s %s id=%d \n",i,wsk->imie,wsk->nazwisko,wsk->telefon,wsk->ulica,wsk->poczta,wsk->miasto,wsk->id);
            wsk=wsk->next;
            i++;
        };
    }
    else{
        printf("Lista kontaktow jest pusta");
    }
}
/*!
*\brief tworzy  wskaznik na strukture nastepnie wysyla go do funkcji odczytaj_z_pliku (oryginal) i wydrukuj  (kopie)
*funkcja nie przyjmuje zadnych parametrow
*/
