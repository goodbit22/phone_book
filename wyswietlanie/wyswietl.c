
#include "wyswietl.h"
#include "wyswietlanie_standardowe.h"

/*!
*\brief funkcja swap zamienia miejscami 
*
\param struct list** list1
\param  struct lista** list2
\details parametry podwoj wskaznik  umozliwia operacje na oryginalnej strukturze 
*/

void swap(struct lista** list1,struct lista** list2){
    struct lista* wsk=(struct lista*)malloc(sizeof(struct lista));
    wsk->id=(*list1)->id;
    strcpy(wsk->imie,(*list1)->imie);
    strcpy(wsk->nazwisko,(*list1)->nazwisko);
    strcpy(wsk->miasto,(*list1)->miasto);
    strcpy(wsk->ulica,(*list1)->ulica);
    strcpy(wsk->telefon,(*list1)->telefon);
    strcpy(wsk->poczta,(*list1)->poczta);
    (*list1)->id=(*list2)->id;
    strcpy((*list1)->imie,(*list2)->imie);
    strcpy((*list1)->nazwisko,(*list2)->nazwisko);
    strcpy((*list1)->miasto,(*list2)->miasto);
    strcpy((*list1)->ulica,(*list2)->ulica);
    strcpy((*list1)->telefon,(*list2)->telefon);
    strcpy((*list1)->poczta,(*list2)->poczta);
    (*list2)->id=wsk->id;
    strcpy((*list2)->imie,wsk->imie);
    strcpy((*list2)->nazwisko,wsk->nazwisko);
    strcpy((*list2)->miasto,wsk->miasto);
    strcpy((*list2)->ulica,wsk->ulica);
    strcpy((*list2)->telefon,wsk->telefon);
    strcpy((*list2)->poczta,wsk->poczta);

    free(wsk);
};

/*!
*\brief funkcja wyswietla jakie ma byc sortowanie
*funckja nie przyjmuje zadnych parametrow
*/
int sortowanie_menu(void){

	int wybor=0;
	printf("1.Malejaco\n");
	printf("2.Rosnaco\n");
	scanf("%d",&wybor);
	if(wybor==1)
        return wybor;
    else{
        if(wybor==2)
            return wybor;
        else
            return -1;
    }
}
/*!
*\brief funkcja wyswietla po czym ma sortowac
*funckja nie przyjmuje zadnych parametrow
*/
int poczym_sortowac(void){
	int wybor=0;
	puts("Po czym mam sortowac");
	puts("1.Po polu id");
	puts("2.Po  polu imie");
	puts("3.Po polu nazwisko");
	puts("4.Po polu telefon");
	puts("5.Po polu miasto");
	puts("6.Po polu ulica");
	puts("7.Po polu poczta");
	scanf("%d",&wybor);
	if(wybor>0){
        if(wybor<8) return wybor;
        else return -1;
	}
	else
        return -1;
}
/*!
*\brief funkcja sortuje kontakty
*
\param struct list** kontakt
\details wskaznik na strukture list
*/
struct lista* posortowana_lista(struct lista** kontakt){
    int typ=-1, kierunek=-1;
    struct lista* posortowana=(*kontakt);
    struct lista* posortowana2=(*kontakt);//=(struct lista*)malloc(sizeof(struct lista));
    //posortowana->id=0;
    //posortowana->prev=NULL;
    //posortowana->next=NULL;
    while(typ==-1){
        typ=poczym_sortowac();
    };
    while(kierunek==-1){
        kierunek=sortowanie_menu();
    };

    switch (kierunek){
    case 1:
        switch(typ){
        case 1:

            while(posortowana2!=NULL){
            while(posortowana!=NULL){
                    if(posortowana->id!=0){
                        if((*kontakt)->id<posortowana->id){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if((*kontakt)->id>posortowana->id){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(wsk->id<posortowana->id){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;
                    }
                    else{
                        posortowana=posortowana->next;
                    }



            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;
            case 2:
            while(posortowana2!=NULL){
            while(posortowana!=NULL){

if(posortowana->id!=0){
                    if(strcmp((*kontakt)->imie,posortowana->imie)<0){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if(strcmp((*kontakt)->imie,posortowana->imie)>0){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(strcmp(wsk->imie,posortowana->imie)<0){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;

            }
            else{
                        posortowana=posortowana->next;
                    }
            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;
        case 3:
            while(posortowana2!=NULL){
            while(posortowana!=NULL){

if(posortowana->id!=0){
                    if(strcmp((*kontakt)->nazwisko,posortowana->nazwisko)<0){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if(strcmp((*kontakt)->nazwisko,posortowana->nazwisko)>0){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(strcmp(wsk->nazwisko,posortowana->nazwisko)<0){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;

            }
            else{
                        posortowana=posortowana->next;
                    }
            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;
        case 4:
            while(posortowana2!=NULL){
            while(posortowana!=NULL){

if(posortowana->id!=0){
                    if(strcmp((*kontakt)->telefon,posortowana->telefon)<0){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if(strcmp((*kontakt)->telefon,posortowana->telefon)>0){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(strcmp(wsk->telefon,posortowana->telefon)<0){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;

            }
            else{
                        posortowana=posortowana->next;
                    }
            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;
        case 5:
            while(posortowana2!=NULL){
            while(posortowana!=NULL){

if(posortowana->id!=0){
                    if(strcmp((*kontakt)->miasto,posortowana->miasto)<0){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if(strcmp((*kontakt)->miasto,posortowana->miasto)>0){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(strcmp(wsk->miasto,posortowana->miasto)<0){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;

            }
            else{
                        posortowana=posortowana->next;
                    }

            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;
        case 6:
            while(posortowana2!=NULL){
            while(posortowana!=NULL){

if(posortowana->id!=0){
                    if(strcmp((*kontakt)->ulica,posortowana->ulica)<0){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if(strcmp((*kontakt)->ulica,posortowana->ulica)>0){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(strcmp(wsk->ulica,posortowana->ulica)<0){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;

            }
            else{
                        posortowana=posortowana->next;
                    }

            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;
        case 7:
            while(posortowana2!=NULL){
            while(posortowana!=NULL){
if(posortowana->id!=0){

                    if(strcmp((*kontakt)->poczta,posortowana->poczta)<0){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if(strcmp((*kontakt)->poczta,posortowana->poczta)>0){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(strcmp(wsk->poczta,posortowana->poczta)<0){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;

            }
            else{
                        posortowana=posortowana->next;
                    }

            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;

        };

    case 2:
        switch(typ){
        case 1:

            while(posortowana2!=NULL){
            while(posortowana!=NULL){
                    if(posortowana->id!=0){
                        if((*kontakt)->id>posortowana->id){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if((*kontakt)->id<posortowana->id){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(wsk->id>posortowana->id){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;
                    }
                    else{
                        posortowana=posortowana->next;
                    }



            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;
            case 2:
            while(posortowana2!=NULL){
            while(posortowana!=NULL){


                    if(strcmp((*kontakt)->imie,posortowana->imie)>0){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if(strcmp((*kontakt)->imie,posortowana->imie)<0){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(strcmp(wsk->imie,posortowana->imie)>0){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;



            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;
        case 3:
            while(posortowana2!=NULL){
            while(posortowana!=NULL){


                    if(strcmp((*kontakt)->nazwisko,posortowana->nazwisko)>0){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if(strcmp((*kontakt)->nazwisko,posortowana->nazwisko)<0){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(strcmp(wsk->nazwisko,posortowana->nazwisko)>0){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;



            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;
        case 4:
            while(posortowana2!=NULL){
            while(posortowana!=NULL){


                    if(strcmp((*kontakt)->telefon,posortowana->telefon)>0){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if(strcmp((*kontakt)->telefon,posortowana->telefon)<0){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(strcmp(wsk->telefon,posortowana->telefon)>0){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;


            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;
        case 5:
            while(posortowana2!=NULL){
            while(posortowana!=NULL){


                    if(strcmp((*kontakt)->miasto,posortowana->miasto)>0){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if(strcmp((*kontakt)->miasto,posortowana->miasto)<0){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(strcmp(wsk->miasto,posortowana->miasto)>0){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;

            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;
        case 6:
            while(posortowana2!=NULL){
            while(posortowana!=NULL){


                    if(strcmp((*kontakt)->ulica,posortowana->ulica)>0){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if(strcmp((*kontakt)->ulica,posortowana->ulica)<0){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(strcmp(wsk->ulica,posortowana->ulica)>0){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;



            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;
        case 7:
            while(posortowana2!=NULL){
            while(posortowana!=NULL){


                    if(strcmp((*kontakt)->poczta,posortowana->poczta)>0){
                        swap(kontakt,&posortowana);
                    }
                    else{
                        if(strcmp((*kontakt)->poczta,posortowana->poczta)<0){
                            struct lista* wsk=(*kontakt);
                            while(wsk->next!=NULL){
                                if(strcmp(wsk->poczta,posortowana->poczta)>0){
                                    swap(&wsk,&posortowana);
                                    break;
                                }
                                else{
                                    wsk=wsk->next;
                                }
                            }
                        }
                    };
                    posortowana=posortowana->next;



            };
            posortowana2=posortowana2->next;
            posortowana=(*kontakt);
            }
            break;

        };
    };
    return *kontakt;
};
/*!
*\brief funckja przypiszuje zmiennej sorted   wartosc funkcji posortowana_lista  i tworzony dla nich miejsce w pamieci
*funkcja nie przyjmuje zadnych parametrow
*/
