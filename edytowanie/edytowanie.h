#ifndef EDYTOWANIE_H_INCLUDED
#define EDYTOWANIE_H_INCLUDED
#include "../struktura.h"
#include "../wyswietlanie/wyswietlanie_standardowe.h"

void edytuj(struct lista** kontakt);
void menu_edytowanie(struct lista** kontakt);

#endif // EDYTOWANIE_H_INCLUDED
