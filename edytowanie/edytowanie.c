
#include "edytowanie.h"

/*!
* \authors Hagaven goodbit22
* \date 2019-03-31
* \section intro_sec Opis
*edytowanie kontaktow
* \mainpage Ogólne informacje
*/



/*!
*\brief funkcja umozliwia edytowanie kontaktu
*\ param struct lista** kontakt
*\details wskaznika podwojny do struktury umozliwia besposrednie operowanie na  oryginalach pol struktury
*/
void edytuj(struct lista** kontakt){
    int wybor=0;
    printf("Co chcesz edytowac\n1.Imie\n2.Nazwisko\n3.Telefon\n4.Adres\n5.Powrot\n");
    scanf("%d",&wybor);
    switch (wybor){
    case 1:
        printf("Imie:");
        scanf("%s",(*kontakt)->imie);
        break;
    case 2:
        printf("Nazwisko:");
        scanf("%s",(*kontakt)->nazwisko);
        break;
    case 3:
        printf("Telefon:");
        scanf("%s",(*kontakt)->telefon);
        break;
    case 4:
        printf("Miejscowosc:");
        scanf("%s",(*kontakt)->miasto);
        printf("Ulica:");
        scanf("%s",(*kontakt)->ulica);
        printf("Kod pocztowy:");
        scanf("%s",(*kontakt)->poczta);
        break;
    default:
        break;
    }
}

/*!
*\brief funkcja  wyswietla  komunikat jakie pole chcemy zedytowac
*\ param struct lista** kontakt
*\details wskaznika podwojny do struktury umozliwia besposrednie operowanie na  oryginalach pol struktury
*/
void menu_edytowanie(struct lista** kontakt){
    int wybor;
    wydrukuj((*kontakt));
    printf("Wybierz kontakt ktory chcesz edytowac\n");
    scanf("%d",&wybor);
    struct lista*wsk=(*kontakt);
    int i=0;
    for(i=0;i<wybor;i++){
        wsk=wsk->next;
    };
    edytuj(&wsk);
    wsk=NULL;
    free(wsk);
}
